package base_repo

import (
	"database/sql"
	"gitlab.systems-fd.com/packages/golang/graphql/old/builder/sbuilder/v2"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2"
)

// WithUpdate реализует репозиторий обновления сущностей в БД - Update
type WithUpdate struct {
	PrimaryRepository pg_repo.PgRepositoryInterface
}

// NewUpdate реализует конструктор репозитория Update
func NewUpdate(primaryRepository pg_repo.PgRepositoryInterface) *WithUpdate {
	return &WithUpdate{
		PrimaryRepository: primaryRepository,
	}
}

// UpdateByGraphQlParameters выполняет обновление значений по переданным
// параметрам GraphQL
func (w WithUpdate) UpdateByGraphQlParameters(
	params sbuilder.Parameters,
	tx *sql.Tx,
) (*pg_repo.InsertUpdateResult, error) {
	return w.PrimaryRepository.UpdateEntitiesByGraphQlParameters(params, tx)
}
