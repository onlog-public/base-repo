package base_repo

import (
	"context"
	"database/sql"
	"github.com/pkg/errors"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// WithAll реализует репозиторий All
type WithAll[T Entity] struct {
	SlaveRepository pg_repo.PgRepositoryInterface
}

// NewAll реализует конструктор репозитория All
func NewAll[T Entity](slaveRepository pg_repo.PgRepositoryInterface) *WithAll[T] {
	return &WithAll[T]{
		SlaveRepository: slaveRepository,
	}
}

// GetAll возвращает все доступные сущности в системе
func (w WithAll[T]) GetAll(ctx context.Context, tx *sql.Tx) ([]T, error) {
	result, err := w.SlaveRepository.GetListByCustomSql(ctx, func(sql string) (result string, args []interface{}) {
		return sql, nil
	}, tx)

	if nil != err {
		return nil, errors.Wrap(err, `[GetAll[`+h.EntityName[T]()+`]]`)
	}

	return h.InterfaceToType[T](result), nil
}
