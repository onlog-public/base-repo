package base_repo

import (
	"database/sql"
	"gitlab.systems-fd.com/packages/golang/graphql/old/builder/sbuilder/v2"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2"
)

// WithInsert реализует репозиторий для вставки сущностей Insert
type WithInsert struct {
	PrimaryRepository pg_repo.PgRepositoryInterface
}

// NewInsert реализует конструктор репозитория Insert
func NewInsert(primaryRepository pg_repo.PgRepositoryInterface) *WithInsert {
	return &WithInsert{
		PrimaryRepository: primaryRepository,
	}
}

// InsertByGraphQlParameters выполняет вставку значений по переданным
// параметрам GraphQL
func (w WithInsert) InsertByGraphQlParameters(
	params sbuilder.Parameters,
	tx *sql.Tx,
) (*pg_repo.InsertUpdateResult, error) {
	return w.PrimaryRepository.InsertEntitiesByGraphQlParameters(params, tx)
}
