package base_repo

import (
	"context"
	"database/sql"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2/events"
)

// WithRepositorySubscription реализует функционал подписок на события
// на основе репозитория PgRepo
type WithRepositorySubscription struct {
	PrimaryRepository pg_repo.PgRepositoryInterface
}

// NewRepositorySubscription реализует конструктор части репозитория
func NewRepositorySubscription(primaryRepository pg_repo.PgRepositoryInterface) *WithRepositorySubscription {
	return &WithRepositorySubscription{
		PrimaryRepository: primaryRepository,
	}
}

// SubscribeToEvents выполняет регистрацию подписчика на события
func (r WithRepositorySubscription) SubscribeToEvents(subscriber events.EventsSubscriberInterface) {
	r.PrimaryRepository.SubscribeToEvents(subscriber)
}

// DispatchEvents выполняет отправку событий в зарегистрированных
// подписчиков. Для внутренних нужд.
func (r WithRepositorySubscription) DispatchEvents(ctx context.Context, events []events.Event, tx *sql.Tx) error { //TODO implement me
	return r.PrimaryRepository.DispatchEvents(ctx, events, tx)
}
