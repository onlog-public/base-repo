package base_repo

import (
	"context"
	"database/sql"
	. "gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2/events"
)

// WithCustomSubscription реализует функционал подписки на события без
// использования стороннего репозитория PgRepo.
type WithCustomSubscription struct {
	subscribers []EventsSubscriberInterface
}

// NewCustomSubscription реализует конструктор репозитория подписок без
// использования стороннего репозитория PgRepo.
func NewCustomSubscription() *WithCustomSubscription {
	return &WithCustomSubscription{
		subscribers: make([]EventsSubscriberInterface, 0),
	}
}

// SubscribeToEvents выполняет регистрацию подписчика на события
func (c *WithCustomSubscription) SubscribeToEvents(subscriber EventsSubscriberInterface) {
	c.subscribers = append(c.subscribers, subscriber)
}

// DispatchEvents выполняет отправку событий в зарегистрированных
// подписчиков. Для внутренних нужд.
func (c *WithCustomSubscription) DispatchEvents(ctx context.Context, events []Event, tx *sql.Tx) error {
	var executedSubscribers []EventsSubscriberInterface
	for _, subscriber := range c.subscribers {
		executedSubscribers = append(executedSubscribers, subscriber)
		for _, event := range events {
			err := subscriber.Process(ctx, event, tx)
			if nil == err {
				continue
			}

			for _, executed := range executedSubscribers {
				executed.Rollback(ctx, err)
			}

			return err
		}
	}

	return nil
}
