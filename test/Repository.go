package test

import . "gitlab.com/onlog-public/base-repo"

// Repository реализует репозиторий, содержащий библиотечные методы
type Repository struct {
	*WithAggregator
	*WithGetter[TEntity]
	*WithAll[TEntity]
	*WithGetByID[TEntity]
	*WithRepositorySubscription
	*WithInsert
	*WithUpdate
	*WithDelete
}

// NewRepository реализует конструктор репозитория
func NewRepository() *Repository {
	master, slave := MakeRepo[TEntity](EntityObj, nil)

	return &Repository{
		WithAggregator:             NewAggregator(slave),
		WithGetter:                 NewGetter[TEntity](slave),
		WithAll:                    NewAll[TEntity](slave),
		WithGetByID:                NewGetByID[TEntity](slave),
		WithRepositorySubscription: NewRepositorySubscription(master),
		WithInsert:                 NewInsert(master),
		WithUpdate:                 NewUpdate(master),
		WithDelete:                 NewDelete(master),
	}
}
