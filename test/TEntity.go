package test

import (
	"github.com/graphql-go/graphql"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
	"strconv"
)

// TEntity реализует тестовую сущность для репозитория
type TEntity struct {
	Id int64
}

// GetPrimaryKey возвращает первичный ключ сущности
func (t TEntity) GetPrimaryKey() string {
	return strconv.FormatInt(t.Id, 10)
}

// GetEntityTable возвращает таблицу, по которой работают
// подписки по сущности
func (t TEntity) GetEntityTable() string {
	return "test"
}

// GetEntityType возвращает тип сущности из шины событий,
// который относятся к данной сущности
func (t TEntity) GetEntityType() string {
	return h.EntityName[TEntity]()
}

// GetPrimaryKeyColumn возвращает колонку, содержащую первичный ключ сущности
func (t TEntity) GetPrimaryKeyColumn() string {
	return "id"
}

// EntityObj содержит GraphQL объект сущности
var EntityObj = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "TEntity",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.ID,
				Name:        "id",
				Description: "ID",
			},
		},
		Description: "Сущность",
	},
)
