package base_repo

import (
	"database/sql"
	"gitlab.systems-fd.com/packages/golang/graphql/old/builder/sbuilder/v2"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2/aggregation/aggregationConverter"
)

// WithAggregator реализует репозиторий агрегации сущностей Aggregator
type WithAggregator struct {
	SlaveRepository pg_repo.PgRepositoryInterface
}

// NewAggregator реализует конструктор репозитория Aggregator
func NewAggregator(slaveRepository pg_repo.PgRepositoryInterface) *WithAggregator {
	return &WithAggregator{
		SlaveRepository: slaveRepository,
	}
}

// GetAggregationByGraphQlParameters возвращает результаты агрегации
// сущностей по переданным параметрам GraphQL
func (w WithAggregator) GetAggregationByGraphQlParameters(
	params sbuilder.Parameters,
	tx *sql.Tx,
) ([]aggregationConverter.Aggregation, error) {
	return w.SlaveRepository.GetAggregationByGraphQlParameters(params, tx)
}
