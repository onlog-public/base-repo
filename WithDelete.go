package base_repo

import (
	"database/sql"
	"gitlab.systems-fd.com/packages/golang/graphql/old/builder/sbuilder/v2"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2"
)

// WithDelete реализует репозиторий для удаления сущностей Delete
type WithDelete struct {
	PrimaryRepository pg_repo.PgRepositoryInterface
}

// NewDelete реализует конструктор репозитория Delete
func NewDelete(primaryRepository pg_repo.PgRepositoryInterface) *WithDelete {
	return &WithDelete{
		PrimaryRepository: primaryRepository,
	}
}

// DeleteByGraphQlParameters выполняет удаление значений по переданным
// параметрам GraphQL
func (w WithDelete) DeleteByGraphQlParameters(
	params sbuilder.Parameters,
	tx *sql.Tx,
) (*pg_repo.DeleteResult, error) {
	return w.PrimaryRepository.DeleteEntitiesByGraphQlParameters(params, tx)
}
