package base_repo

import (
	"context"
	"database/sql"
	"gitlab.com/onlog-public/repo-subscribers"
	"gitlab.systems-fd.com/packages/golang/graphql/old/builder/sbuilder/v2"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2/aggregation/aggregationConverter"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2/events"
)

// Entity описывает интерфейс сущности, используемой для
// реализации работы с репозиториями.
type Entity interface {
	repo_subscribers.EntityWithSubscription

	// GetPrimaryKeyColumn возвращает колонку, содержащую первичный ключ сущности
	GetPrimaryKeyColumn() string
}

// Getter описывает интерфейс репозитория, содержащего метод получения листинга
// сущностей по параметрам GraphQL.
type Getter[T Entity] interface {
	// GetListByGraphQlParameters возвращает листинг сущностей по
	// переданным параметрам GraphQL
	GetListByGraphQlParameters(
		params sbuilder.Parameters,
		tx *sql.Tx,
	) ([]T, error)
}

// All описывает интерфейс репозитория, содержащего метод получения всех
// доступных сущностей системы
type All[T Entity] interface {
	// GetAll возвращает все доступные сущности в системе
	GetAll(ctx context.Context, tx *sql.Tx) ([]T, error)
}

// Aggregator описывает интерфейс репозитория, содержащего метод получения
// агрегации сущностей.
type Aggregator interface {
	// GetAggregationByGraphQlParameters возвращает результаты агрегации
	// сущностей по переданным параметрам GraphQL
	GetAggregationByGraphQlParameters(
		params sbuilder.Parameters,
		tx *sql.Tx,
	) ([]aggregationConverter.Aggregation, error)
}

// Insert описывает интерфейс репозитория, содержащего метод вставки
// сущностей по переданным параметрам GraphQL.
type Insert interface {
	// InsertByGraphQlParameters выполняет вставку значений по переданным
	// параметрам GraphQL
	InsertByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) (*pg_repo.InsertUpdateResult, error)
}

// Update описывает интерфейс репозитория, содержащего метод обновления
// сущностей по переданным параметрам GraphQL.
type Update interface {
	// UpdateByGraphQlParameters выполняет обновление значений по переданным
	// параметрам GraphQL
	UpdateByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) (*pg_repo.InsertUpdateResult, error)
}

// Delete описывает интерфейс репозитория, содержащего метод удаления
// сущностей по переданным параметрам GraphQL.
type Delete interface {
	// DeleteByGraphQlParameters выполняет удаление значений по переданным
	// параметрам GraphQL
	DeleteByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) (*pg_repo.DeleteResult, error)
}

// Subscription описывает интерфейс репозитория для работы с подписками
// на события внутри репозитория. Описывает методы, эквивалентные базовым,
// используемым в PgRepo.
type Subscription interface {
	// SubscribeToEvents выполняет регистрацию подписчика на события
	SubscribeToEvents(subscriber events.EventsSubscriberInterface)

	// DispatchEvents выполняет отправку событий в зарегистрированных
	// подписчиков. Для внутренних нужд.
	DispatchEvents(ctx context.Context, events []events.Event, tx *sql.Tx) error
}
