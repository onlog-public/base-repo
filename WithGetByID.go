package base_repo

import (
	"context"
	"database/sql"
	"github.com/lib/pq"
	"github.com/pkg/errors"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// WithGetByID реализует репозиторий, содержащий метод получения
// данных по ID, в соответствии с интерфейсом repo_subscribers.EntityBaseGetter
type WithGetByID[T Entity] struct {
	SlaveRepository pg_repo.PgRepositoryInterface
}

// NewGetByID реализует конструктор репозитория repo_subscribers.EntityBaseGetter
func NewGetByID[T Entity](slaveRepository pg_repo.PgRepositoryInterface) *WithGetByID[T] {
	return &WithGetByID[T]{
		SlaveRepository: slaveRepository,
	}
}

// GetByID возвращает сущности по переданным идентификаторам
func (w WithGetByID[T]) GetByID(ctx context.Context, id []string, tx *sql.Tx) ([]T, error) {
	if 0 == len(id) {
		return nil, nil
	}

	var entity T
	result, err := w.SlaveRepository.GetListByCustomSql(ctx, func(sql string) (result string, args []interface{}) {
		return sql + ` where ` + entity.GetPrimaryKeyColumn() + `=any($1)`, []any{pq.Array(id)}
	}, tx)

	if nil != err {
		return nil, errors.Wrap(err, `[GetByID[`+h.EntityName[T]()+`]]`)
	}

	return h.InterfaceToType[T](result), nil
}
