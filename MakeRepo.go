package base_repo

import (
	"github.com/graphql-go/graphql"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	. "gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
	"reflect"
)

// MakeRepo реализует фабрику репозиториев PgRepo на основе типизации
// сущности на дженериках. Метод генерирует репозитории и возвращает
// их, в случае успеха.
// Метод вызывает фатальную ошибку, если во время генерации репозитория
// произошла ошибка.
// Метод принимает на вход подключения к мастеру и к slave для реализации
// распределения запросов между репликами базы данных.
// Метод использует подключение к мастеру даже для slave, если не передано
// дополнительное подключение.
func MakeRepo[T Entity](
	graphQlObject *graphql.Object,
	masterConnection *sqlx.DB,
	slaveConnection ...*sqlx.DB,
) (master PgRepositoryInterface, slave PgRepositoryInterface) {
	var entity T
	var err error
	logger := logrus.WithField(`prefix`, `[Repository[`+h.EntityName[T]()+`]]`)

	master, err = NewPgRepository(
		graphQlObject,
		reflect.TypeOf(entity),
		entity.GetEntityTable(),
		entity.GetPrimaryKeyColumn(),
		masterConnection,
	)
	if err != nil {
		logger.WithError(err).Fatal(`Failed to create master repository`)
		return
	}

	slaveConn := masterConnection
	if 0 != len(slaveConnection) {
		slaveConn = slaveConnection[0]
	}

	slave, err = NewPgRepository(
		graphQlObject,
		reflect.TypeOf(entity),
		entity.GetEntityTable(),
		entity.GetPrimaryKeyColumn(),
		slaveConn,
	)
	if err != nil {
		logger.WithError(err).Fatal(`Failed to create slave repository`)
		return
	}

	return
}
