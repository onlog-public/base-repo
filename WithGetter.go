package base_repo

import (
	"database/sql"
	"github.com/pkg/errors"
	"gitlab.systems-fd.com/packages/golang/graphql/old/builder/sbuilder/v2"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// WithGetter реализует репозиторий Getter
type WithGetter[T Entity] struct {
	SlaveRepository pg_repo.PgRepositoryInterface
}

// NewGetter реализует конструктор репозитория Getter
func NewGetter[T Entity](slaveRepository pg_repo.PgRepositoryInterface) *WithGetter[T] {
	return &WithGetter[T]{
		SlaveRepository: slaveRepository,
	}
}

// GetListByGraphQlParameters возвращает листинг сущностей по
// переданным параметрам GraphQL
func (w WithGetter[T]) GetListByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) ([]T, error) {
	result, err := w.SlaveRepository.GetListByGraphQlParameters(params, tx)
	if nil != err {
		return nil, errors.Wrap(err, `[GetListByGraphQlParameters[`+h.EntityName[T]()+`]]`)
	}

	return h.InterfaceToType[T](result), nil
}
